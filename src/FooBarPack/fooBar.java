package FooBarPack;

 public class fooBar {
	 
	 private static final int FOO = 3;
	 private static final int BAR = 5;

	// Entry Point
	public  static String CheckFoobar(int num)
	{
		if(IsFoo(num) && IsBar(num))
			return "foobar";
		else if(IsFoo(num))
			return "foo";
		else if(IsBar(num))
			return "bar";		
		else
		return Integer.toString(num);	
	}
	
	// Helper to check whether number contains the given digit
	private static boolean containsDigit(int number, int digit){
		String numberString = Integer.toString(number);
		return numberString.contains(Integer.toString(digit));
	}
	
	// Checks whether a given is multiple of 3 or contains 3
	private static boolean IsFoo(int number){
		if(number > 0) {
			if(number % FOO == 0 || containsDigit(number, FOO))
				return true;	
		}
		return false;
		
	}
	
	// Checks whether a given is multiple of 5 or contains 5
	private static boolean IsBar(int number) {
		if(number > 0) {
			if(number % BAR == 0 || containsDigit(number, BAR))
				return true;	
		}
		return false;
	}
	
		
	public static void main(String args[]){
				
		for(int i=1;i<=100;i++)
		{
			System.out.println(CheckFoobar(i));
		}
	}
	
}
