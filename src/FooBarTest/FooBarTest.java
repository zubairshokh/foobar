package FooBarTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import FooBarPack.fooBar;

public class FooBarTest {

	@Test
	public void Given_three_returns_foo(){
		
	String result=	fooBar.CheckFoobar(3);
	assertEquals("foo",result);	
	}
	
	@Test
	public void Given_five_returns_bar(){
		
	String result=	fooBar.CheckFoobar(5);
	assertEquals("bar",result);	
		
	}
	
	@Test
	public void Given_multiples_of_three_returns_foo(){
		
	String result=	fooBar.CheckFoobar(9);
	assertEquals("foo",result);	
		
	}
	
	@Test
	public void Given_multiples_of_five_returns_bar(){
		
	String result=	fooBar.CheckFoobar(20);
	assertEquals("bar",result);	
		
	}
	
	
	@Test
	public void Given_zero_return_zero(){
		
	String result=	fooBar.CheckFoobar(0);
	assertEquals("0",result);	
		
	}
	

	@Test
	public void Given_multiple_of_three_as_well_as_five_return_foobar(){
		
		String result=fooBar.CheckFoobar(15);
		
		assertEquals("foobar",result);
		
	}
	
	@Test
	public void Given_number_that_contain_three_returns_foo(){
		String result=fooBar.CheckFoobar(33);
		assertEquals("foo", result);
		
	}
	
	@Test
	public void Given_number_that_contain_five_returns_bar(){
		String result=fooBar.CheckFoobar(25);
		assertEquals("bar", result);
		
	}
	
	@Test
	public void Given_number_that_contains_three_and_five_returns_foobar(){
		String result=fooBar.CheckFoobar(53);
		assertEquals("foobar", result);
	}
	
	@Test
	public void Given_number_that_is_not_multiple_of_three_or_five_returns_null(){
		String result=fooBar.CheckFoobar(11);
		assertEquals("11",result);
	}
	
}
